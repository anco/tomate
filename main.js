import { loadMap } from './modules/map.js';
import { initializeLineView } from './modules/lines.js'

// Globals
initializeLineView()
// Map

loadMap()

// Drawer
const drawer = document.querySelector('.drawer-scrolling');
const openButton = document.getElementById('lines-button')
const closeButton = drawer.querySelector('sl-button[variant="primary"]');
const drawerScrollable = document.getElementById('inner-drawer');

openButton.addEventListener('click', () => {
  openButton.style.display = 'none'; // for some reason hidden = true does not work
  drawer.show();
});
closeButton.addEventListener('click', () => drawer.hide());

drawer.addEventListener('sl-hide', () => openButton.style.display = 'block');

const dialog = document.querySelector('.dialog-faq');
const dialogOpenButton = document.querySelector('.faq-button');
const dialogCloseButton = dialog.querySelector('sl-button[slot="footer"]');

dialogOpenButton.addEventListener('click', () => dialog.show());
dialogCloseButton.addEventListener('click', () => dialog.hide());