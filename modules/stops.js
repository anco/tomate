import { loadSchedule } from './data.js'
import { createDaySelect, getShortName, dayTranslation } from './utils.js';

function createRouteHeader(routeName) {
  const header = document.createElement('tomate-route-header');

  header.innerText = routeName;

  return header;
}

function createStopList(data, routeId, direction) {
  const allButtons = [];
  const list = document.createElement('ol');
  list.classList.toggle('stop-list');

  for (const [index, stop] of data.entries()) {
    const item = document.createElement('li');
    const stopButton = document.createElement('sl-button');
    const stopSuffix = document.createElement('sl-tag');

    stopButton.outline = true;
    stopButton.classList.toggle('stop-button');
    stopButton.innerText = stop.stop_name;
    stopButton.code = stop.code;
    stopButton.lineId = stop.line

    stopSuffix.innerText = index;
    stopSuffix.variant = 'neutral'
    stopSuffix.size = 'small'
    stopSuffix.pill = true
    stopSuffix.slot = 'suffix'

    stopButton.appendChild(stopSuffix)

    item.appendChild(stopButton);
    list.appendChild(item);

    allButtons.push(stopButton);
  }


  for (const button of allButtons) {
    button.addEventListener('click', (e) => {
      allButtons.forEach(b => {
        if (b.classList.contains('outline-button-selected')) {
          b.classList.toggle('outline-button-selected');
        }
      });
      button.classList.toggle('outline-button-selected');

      pushScheduleList(button.innerText, button.lineId, button.code, direction);

      // Stop Marker events
      const marker = document.getElementById(button.code);
      const otherMarkers = document.querySelectorAll('.stop-marker');
      for (const marker of otherMarkers) {
        if (marker.id != button.code) {
          marker.dispatchEvent(new CustomEvent('tomate-stop-deselected', { detail: { code: marker.id } }));
        }
      }
      marker.dispatchEvent(new CustomEvent('tomate-stop-selected', { detail: { code: button.code } }));
    });
  }

  return list;
}

async function pushScheduleList(stopName, lineId, scheduleCode, direction) {
  const schedule = await loadSchedule(lineId, scheduleCode, direction);
  const nav = document.querySelector('.line-navigation');
  const scheduleList = document.createElement('sl-carousel-item');
  const scheduleListInner = document.createElement('div');
  const backButton = document.createElement('tomate-back-button');
  const daySelector = createDaySelect('day-selector')

  scheduleList.id = 'schedule-list';
  scheduleListInner.id = 'schedule-list-inner';
  scheduleListInner.classList.toggle('schedule-list-inner');

  backButton.innerText = stopName.replace(/\n[0-9]+/, '');

  backButton.addEventListener('click', () => {
    nav.previous();
  });
  daySelector.currentDay = 'monday';
  daySelector.addEventListener('sl-change', (e) => {
    daySelector.currentDay[e.target.value]

    populateSchedules(scheduleListInner, schedule[e.target.value], schedule.name)
  })

  scheduleListInner.appendChild(backButton);
  scheduleListInner.appendChild(daySelector);

  populateSchedules(scheduleListInner, schedule[daySelector.currentDay], schedule.name)

  scheduleList.appendChild(scheduleListInner)
  nav.appendChild(scheduleList);
  nav.next();
  nav.updateComplete.then(() => {
    // Hack to get the next slide into view
    // when the previous one was scrolled down
    setTimeout(() => {
      scheduleList.scrollIntoView();
    }, 300);
  });
}

function populateSchedules(container, schedule, scheduleName) {
  container.querySelectorAll('.schedule-list-item').forEach(child => container.removeChild(child));
  const alert = document.getElementById('tomate-schedule-alert');
  if (alert) {
    container.removeChild(alert);
  }

  if (schedule.length == 0) {
    const alert = document.createElement('sl-alert');
    const icon = document.createElement('sl-icon');
    icon.name = 'info-circle';

    alert.open = true;
    alert.appendChild(icon);
    alert.innerText = 'No hay horarios para esta línea en este día.';
    alert.id = 'tomate-schedule-alert'

    container.appendChild(alert);

  } else {
    for (const hour of schedule) {
      const scheduleListItem = document.createElement('div');
      const scheduleListItemPrefix = document.createElement('span');
      const scheduleListItemName = document.createElement('span');
      const scheduleListItemSuffix = document.createElement('span');

      scheduleListItemPrefix.innerText = getShortName(scheduleName);
      scheduleListItemPrefix.classList.toggle('schedule-list-item-prefix');

      scheduleListItemName.innerText = scheduleName;
      scheduleListItemName.classList.toggle('schedule-list-item-name');

      scheduleListItemSuffix.innerText = hour;
      scheduleListItemSuffix.classList.toggle('schedule-list-item-suffix');

      scheduleListItem.classList.toggle('schedule-list-item');
      scheduleListItem.appendChild(scheduleListItemPrefix);
      scheduleListItem.appendChild(scheduleListItemName);
      scheduleListItem.appendChild(scheduleListItemSuffix);

      container.appendChild(scheduleListItem);
    }
  }
}

export { createRouteHeader, createStopList }