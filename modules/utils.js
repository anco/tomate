function createBackButton(lineData, onClick) {
  const backButton = document.createElement('tomate-back-button');

  backButton.prefixLabel = lineData.name.replaceAll(' ', '');

  backButton.innerText = getFullName(lineData.name);

  backButton.addEventListener('click', () => onClick())

  return backButton;
}

function createDaySelect(selectClass) {
  const options = ['monday', 'tuesday', 'thursday', 'friday', 'saturday', 'sunday', 'holiday'];
  return createSelect(options, selectClass, 'calendar-day');
}

function createSelect(options, selectClass, iconName) {
  const select = document.createElement('sl-select');
  const prefix = document.createElement('sl-icon');

  prefix.slot = 'prefix'
  prefix.name = iconName

  select.classList.toggle(selectClass);
  select.value = options[0];

  select.appendChild(prefix)

  for (const option of options) {
    const optionItem = document.createElement('sl-option');
    const optionTL = dayTranslation(option)
    optionItem.value = option.trim();
    optionItem.innerText = optionTL;
    select.appendChild(optionItem);
  }

  return select;
}

function getFullName(lineCode) {
  return lineCode.replace('R', 'Ramal').replace('T', 'Troncal')
}

function getShortName(name) {
  return name.replace('Ramal ', 'R').replace('Troncal ', 'T').replace(' Domingo', 'D')
}

function dayTranslation(day) {
  switch (day) {
    case 'monday':
      return 'Lunes';
    case 'tuesday':
      return 'Martes';
    case 'wednesday':
      return 'Miércoles';
    case 'thursday':
      return 'Jueves';
    case 'friday':
      return 'Viernes';
    case 'saturday':
      return 'Sábado';
    case 'sunday':
      return 'Domingo';
    case 'holiday':
      return 'Feriados';
    default:
      return day;
  }
}

function getMapInitialPosition() {
  let sizeClass = 'desktop'
  if (window.screen.availWidth < 400) {
    sizeClass = 'phone-potrait'
  } else if (window.screen.availWidth < 800) {
    sizeClass = 'phone-landscape'
  }

  let padding = 160
  let offsetX = 100
  let offsetY = -10

  switch (sizeClass) {
    case 'desktop':
      padding = 160
      offsetX = 100
      offsetY = -10
      break;

    case 'phone-potrait':
      padding = 20
      offsetX = 0
      offsetY = -10
      break;

    case 'phone-landscape':
      padding = 100
      offsetX = 100
      offsetY = -10
      break;

    default:
      break;
  }

  return { padding, offsetX, offsetY }
}

export { createBackButton, createSelect, createDaySelect, getFullName, getShortName, dayTranslation, getMapInitialPosition }