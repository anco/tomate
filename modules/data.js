async function loadLinesAndRoutes() {
  return await fetch('./data/linesAndRoutes.json')
    .then(response => response.json());
}

async function loadRoute(id) {
  return await fetch(`./data/routes/route${id}.json`)
    .then(response => response.json());
}

async function loadSchedule(lineId, code, direction) {
  return await fetch(`./data/schedules/schedules-${lineId}-${code}-${direction}-98.json`)
    .then(response => response.json());
}

export { loadLinesAndRoutes, loadRoute, loadSchedule };