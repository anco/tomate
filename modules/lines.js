import { loadLinesAndRoutes, loadRoute } from './data.js';
import { showStopsOnMap, removeSourceAndLayer } from './map.js';
import { createBackButton } from './utils.js';
import { createRouteHeader, createStopList } from './stops.js';

async function initializeLineView() {
  const linesAndRoutes = await loadLinesAndRoutes();
  const input = document.getElementById('line-search-input');
  const container = document.querySelector('.line-list');
  const lineContainers = document.createElement('tomate-lines');
  const lines = [];
  container.appendChild(lineContainers);

  for (const line of linesAndRoutes.lines) {
    for (const route of line.routes) {
      const routeWithStops = await loadRoute(route.id);
      //lineContainers.append(createLineContainer(route, line, routeWithStops))
      lines.push({ routeData: route, lineData: line, stopsData: routeWithStops });
    }
  }

  lineContainers.lines = lines;

  lineContainers.updateComplete.then(() => {
    // Line Navigation
    const lineNavigation = document.querySelector('.line-navigation');
    const lineButtons = lineContainers.shadowRoot.querySelectorAll('tomate-line');

    const removeSlide = () => {
      const slide = lineNavigation.children[lineNavigation.children.length - 1];
      const numSlides = lineNavigation.querySelectorAll('sl-carousel-item').length;

      if (numSlides > 1) {
        // Will be removed by the event listener below (1)
        lineNavigation.previous();
      }
    };

    const addSlide = (className, routeData, lineData, stopsData, direction) => {
      const slide = document.createElement('sl-carousel-item');
      slide.classList.toggle(className);

      // TODO/FIXME: there is an issue where if you select a route down the list
      // the stop list keeps the scroll, we should try to reset it somehow

      slide.appendChild(createBackButton(lineData, removeSlide));
      slide.appendChild(createRouteHeader(routeData.routeName));
      slide.appendChild(createStopList(stopsData, routeData.routeId, direction));

      lineNavigation.appendChild(slide);
      lineNavigation.next();
      lineNavigation.updateComplete.then(() => {
        const scrollContainer = lineNavigation.shadowRoot.getElementById('scroll-container');
        scrollContainer.style.overflow = 'hidden'; //disallow horizontal scroll
        // Hack to get the next slide into view
        // when the previous one was scrolled down
        setTimeout(() => {
          slide.scrollIntoView();
        }, 300);
      });
    };

    lineContainers._clickCallback = (routeData, lineData, stopsData) => {
      input.hidden = true;
      input.style.display = 'none';
      addSlide('stops-slide', { routeName: routeData.name, routeId: routeData.id }, lineData, stopsData.stops, routeData.direction);
      // TODO: improve this passing along of full data
      showStopsOnMap(stopsData.route, stopsData.stops);
    }

    // (1)
    lineNavigation.addEventListener('sl-slide-change', (event) => {
      const child = lineNavigation.children[event.detail.index + 1];
      const input = document.getElementById('line-search-input');
      if (event.detail.index >= 0 && child) {
        child.remove();
        if (event.detail.index == 0) {
          input.hidden = false;
          input.style.display = 'block';
          removeSourceAndLayer('route');
        }
      }
    });
  });

  // Line search input
  const actionButton = document.getElementById('line-search-action');
  actionButton.addEventListener('click', () => lineContainers._filter(input.value))

  input.addEventListener('sl-change', (event) => {
    if (input.value !== event.target.value || event.target.value === '') {
      lineContainers._filter(event.target.value)
    }
  })

  // With help of https://stackoverflow.com/a/5926782
  let typingTimer;
  let doneTypingInterval = 500; //ms 

  //on keyup, start the countdown
  input.addEventListener('keyup', () => {
    clearTimeout(typingTimer);
    if (input.value) {
      //user is "finished typing," do something
      typingTimer = setTimeout(() => lineContainers._filter(input.value), doneTypingInterval);
    }
  });
}
// Line List

function createLineContainer(routeData, lineData, stopsData) {
  const lineContainer = document.createElement('tomate-line');
  // TODO use global datasources and query functions, pass ids along
  lineContainer.lineData = lineData;
  lineContainer.routeName = routeData.name;
  lineContainer.routeId = routeData.id;
  lineContainer.stopsData = stopsData.stops;
  lineContainer.stopsCoordinates = stopsData.route;


  // button content
  lineContainer.innerText = routeData.name;
  lineContainer.linePrefix = lineData.name.replaceAll(' ', '');
  lineContainer.direction = routeData.direction
  lineContainer.lineSuffix = routeData.direction === 'I' ? 'IDA' : 'VUELTA';

  return lineContainer
}

export { initializeLineView }