import { getMapInitialPosition } from './utils.js';

let map = undefined;
let currentMarkers = [];

function loadMap() {
  map = new maplibregl.Map({
    container: 'map',
    // Don't steal my key, there is a free plan :)
    style: 'https://api.maptiler.com/maps/bright-v2/style.json?key=QYZS6KRECh7eBFe951Ub', // stylesheet location
    center: [-64.35878, -33.12299], // starting position [lng, lat]
    zoom: 14, // starting zoom
    customAttribution: '© SATCRC'
  });

  const nav = new maplibregl.NavigationControl();
  map.addControl(nav, 'bottom-right');
}

function showStopsOnMap(stopsCoordinates, stopsData) {
  const color = stopsData[0].direction === 'I' ? 'rgb(14, 165, 233)' : 'tomato'; // sky-800
  let geoJSONCoords = []
  for (const [index, stop] of stopsData.entries()) {
    currentMarkers.push(new maplibregl.Marker({ element: createMarker(index, stop.code, stopsData[0].direction) })
      .setLngLat([parseFloat(stop.lon), parseFloat(stop.lat)])
      .addTo(map));
  }

  for (const stop of stopsCoordinates) {
    geoJSONCoords.push([stop[0], stop[1]]);
  }

  map.addSource('route', {
    'type': 'geojson',
    'data': {
      'type': 'Feature',
      'properties': {},
      'geometry': {
        'type': 'LineString',
        'coordinates': geoJSONCoords
      }
    }
  });

  map.addLayer({
    'id': 'route',
    'type': 'line',
    'source': 'route',
    'layout': {
      'line-join': 'round',
      'line-cap': 'round'
    },
    'paint': {
      'line-color': color,
      'line-width': 6,
    }
  });

  const { padding, offsetX, offsetY } = getMapInitialPosition()

  console.log(window.screen.availWidth, padding, offsetX, offsetY)

  map.fitBounds(getBounds(geoJSONCoords), { padding: padding, offset: [offsetX, offsetY] })
}

function getBounds(coords) {
  let top = Math.min(...coords.flatMap(latlong => latlong[0]))
  let trailing = Math.min(...coords.flatMap(latlong => latlong[1]))
  let leading = Math.max(...coords.flatMap(latlong => latlong[0]))
  let bottom = Math.max(...coords.flatMap(latlong => latlong[1]))

  return [[leading, trailing], [top, bottom]]
}

function removeSourceAndLayer(id) {
  if (map.getLayer(id) && map.getSource(id)) {
    map.removeLayer(id);
    map.removeSource(id);
    for (const marker of currentMarkers) {
      marker.remove();
    }
    currentMarkers = [];
  } else {
    console.error(`No such layer/source with ID: ${id}`);
  }
}

function createMarker(number, code, direction) {
  const marker = document.createElement('div');

  marker.classList.toggle('stop-marker');
  marker.style['border-color'] = direction === 'I' ? 'var(--sl-color-sky-500)' : 'var(--sl-color-tomato-500)'
  marker.style['background-color'] = 'var(--sl-color-gray-50)';
  marker.innerText = number;
  marker.id = code
  marker.direction = direction;
  marker.addEventListener('tomate-stop-selected', function (event) {
    if (event.detail.code === this.id) {
      if (!this.classList.contains('stop-marker-selected')) {
        this.classList.toggle('stop-marker-selected');
        marker.style['border-color'] = direction === 'I' ? 'var(--sl-color-sky-600)' : 'var(--sl-color-tomato-600)';
        marker.style['background-color'] = direction === 'I' ? 'var(--sl-color-sky-500)' : 'var(--sl-color-tomato-500)';
      }
    }
  });

  marker.addEventListener('tomate-stop-deselected', function (event) {
    if (event.detail.code === this.id && this.classList.contains('stop-marker-selected')) {
      this.classList.toggle('stop-marker-selected');
      marker.style['border-color'] = direction === 'I' ? 'var(--sl-color-sky-500)' : 'var(--sl-color-tomato-500)';
      marker.style['background-color'] = 'var(--sl-color-gray-50)';
    }
  });

  return marker;
}

export { loadMap, showStopsOnMap, removeSourceAndLayer }