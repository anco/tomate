/**
 * @license
 * Copyright 2019 Google LLC
 * SPDX-License-Identifier: BSD-3-Clause
 */
const t=window,i=t.ShadowRoot&&(void 0===t.ShadyCSS||t.ShadyCSS.nativeShadow)&&"adoptedStyleSheets"in Document.prototype&&"replace"in CSSStyleSheet.prototype,s=Symbol(),e=new WeakMap;class n{constructor(t,i,e){if(this._$cssResult$=!0,e!==s)throw Error("CSSResult is not constructable. Use `unsafeCSS` or `css` instead.");this.cssText=t,this.t=i}get styleSheet(){let t=this.o;const s=this.t;if(i&&void 0===t){const i=void 0!==s&&1===s.length;i&&(t=e.get(s)),void 0===t&&((this.o=t=new CSSStyleSheet).replaceSync(this.cssText),i&&e.set(s,t))}return t}toString(){return this.cssText}}const o=(t,...i)=>{const e=1===t.length?t[0]:i.reduce(((i,s,e)=>i+(t=>{if(!0===t._$cssResult$)return t.cssText;if("number"==typeof t)return t;throw Error("Value passed to 'css' function must be a 'css' function result: "+t+". Use 'unsafeCSS' to pass non-literal values, but take care to ensure page security.")})(s)+t[e+1]),t[0]);return new n(e,t,s)},r=i?t=>t:t=>t instanceof CSSStyleSheet?(t=>{let i="";for(const s of t.cssRules)i+=s.cssText;return(t=>new n("string"==typeof t?t:t+"",void 0,s))(i)})(t):t
/**
 * @license
 * Copyright 2017 Google LLC
 * SPDX-License-Identifier: BSD-3-Clause
 */;var l;const h=window,a=h.trustedTypes,c=a?a.emptyScript:"",u=h.reactiveElementPolyfillSupport,d={toAttribute(t,i){switch(i){case Boolean:t=t?c:null;break;case Object:case Array:t=null==t?t:JSON.stringify(t)}return t},fromAttribute(t,i){let s=t;switch(i){case Boolean:s=null!==t;break;case Number:s=null===t?null:Number(t);break;case Object:case Array:try{s=JSON.parse(t)}catch(t){s=null}}return s}},v=(t,i)=>i!==t&&(i==i||t==t),f={attribute:!0,type:String,converter:d,reflect:!1,hasChanged:v};class p extends HTMLElement{constructor(){super(),this._$Ei=new Map,this.isUpdatePending=!1,this.hasUpdated=!1,this._$El=null,this.u()}static addInitializer(t){var i;this.finalize(),(null!==(i=this.h)&&void 0!==i?i:this.h=[]).push(t)}static get observedAttributes(){this.finalize();const t=[];return this.elementProperties.forEach(((i,s)=>{const e=this._$Ep(s,i);void 0!==e&&(this._$Ev.set(e,s),t.push(e))})),t}static createProperty(t,i=f){if(i.state&&(i.attribute=!1),this.finalize(),this.elementProperties.set(t,i),!i.noAccessor&&!this.prototype.hasOwnProperty(t)){const s="symbol"==typeof t?Symbol():"__"+t,e=this.getPropertyDescriptor(t,s,i);void 0!==e&&Object.defineProperty(this.prototype,t,e)}}static getPropertyDescriptor(t,i,s){return{get(){return this[i]},set(e){const n=this[t];this[i]=e,this.requestUpdate(t,n,s)},configurable:!0,enumerable:!0}}static getPropertyOptions(t){return this.elementProperties.get(t)||f}static finalize(){if(this.hasOwnProperty("finalized"))return!1;this.finalized=!0;const t=Object.getPrototypeOf(this);if(t.finalize(),void 0!==t.h&&(this.h=[...t.h]),this.elementProperties=new Map(t.elementProperties),this._$Ev=new Map,this.hasOwnProperty("properties")){const t=this.properties,i=[...Object.getOwnPropertyNames(t),...Object.getOwnPropertySymbols(t)];for(const s of i)this.createProperty(s,t[s])}return this.elementStyles=this.finalizeStyles(this.styles),!0}static finalizeStyles(t){const i=[];if(Array.isArray(t)){const s=new Set(t.flat(1/0).reverse());for(const t of s)i.unshift(r(t))}else void 0!==t&&i.push(r(t));return i}static _$Ep(t,i){const s=i.attribute;return!1===s?void 0:"string"==typeof s?s:"string"==typeof t?t.toLowerCase():void 0}u(){var t;this._$E_=new Promise((t=>this.enableUpdating=t)),this._$AL=new Map,this._$Eg(),this.requestUpdate(),null===(t=this.constructor.h)||void 0===t||t.forEach((t=>t(this)))}addController(t){var i,s;(null!==(i=this._$ES)&&void 0!==i?i:this._$ES=[]).push(t),void 0!==this.renderRoot&&this.isConnected&&(null===(s=t.hostConnected)||void 0===s||s.call(t))}removeController(t){var i;null===(i=this._$ES)||void 0===i||i.splice(this._$ES.indexOf(t)>>>0,1)}_$Eg(){this.constructor.elementProperties.forEach(((t,i)=>{this.hasOwnProperty(i)&&(this._$Ei.set(i,this[i]),delete this[i])}))}createRenderRoot(){var s;const e=null!==(s=this.shadowRoot)&&void 0!==s?s:this.attachShadow(this.constructor.shadowRootOptions);return((s,e)=>{i?s.adoptedStyleSheets=e.map((t=>t instanceof CSSStyleSheet?t:t.styleSheet)):e.forEach((i=>{const e=document.createElement("style"),n=t.litNonce;void 0!==n&&e.setAttribute("nonce",n),e.textContent=i.cssText,s.appendChild(e)}))})(e,this.constructor.elementStyles),e}connectedCallback(){var t;void 0===this.renderRoot&&(this.renderRoot=this.createRenderRoot()),this.enableUpdating(!0),null===(t=this._$ES)||void 0===t||t.forEach((t=>{var i;return null===(i=t.hostConnected)||void 0===i?void 0:i.call(t)}))}enableUpdating(t){}disconnectedCallback(){var t;null===(t=this._$ES)||void 0===t||t.forEach((t=>{var i;return null===(i=t.hostDisconnected)||void 0===i?void 0:i.call(t)}))}attributeChangedCallback(t,i,s){this._$AK(t,s)}_$EO(t,i,s=f){var e;const n=this.constructor._$Ep(t,s);if(void 0!==n&&!0===s.reflect){const o=(void 0!==(null===(e=s.converter)||void 0===e?void 0:e.toAttribute)?s.converter:d).toAttribute(i,s.type);this._$El=t,null==o?this.removeAttribute(n):this.setAttribute(n,o),this._$El=null}}_$AK(t,i){var s;const e=this.constructor,n=e._$Ev.get(t);if(void 0!==n&&this._$El!==n){const t=e.getPropertyOptions(n),o="function"==typeof t.converter?{fromAttribute:t.converter}:void 0!==(null===(s=t.converter)||void 0===s?void 0:s.fromAttribute)?t.converter:d;this._$El=n,this[n]=o.fromAttribute(i,t.type),this._$El=null}}requestUpdate(t,i,s){let e=!0;void 0!==t&&(((s=s||this.constructor.getPropertyOptions(t)).hasChanged||v)(this[t],i)?(this._$AL.has(t)||this._$AL.set(t,i),!0===s.reflect&&this._$El!==t&&(void 0===this._$EC&&(this._$EC=new Map),this._$EC.set(t,s))):e=!1),!this.isUpdatePending&&e&&(this._$E_=this._$Ej())}async _$Ej(){this.isUpdatePending=!0;try{await this._$E_}catch(t){Promise.reject(t)}const t=this.scheduleUpdate();return null!=t&&await t,!this.isUpdatePending}scheduleUpdate(){return this.performUpdate()}performUpdate(){var t;if(!this.isUpdatePending)return;this.hasUpdated,this._$Ei&&(this._$Ei.forEach(((t,i)=>this[i]=t)),this._$Ei=void 0);let i=!1;const s=this._$AL;try{i=this.shouldUpdate(s),i?(this.willUpdate(s),null===(t=this._$ES)||void 0===t||t.forEach((t=>{var i;return null===(i=t.hostUpdate)||void 0===i?void 0:i.call(t)})),this.update(s)):this._$Ek()}catch(t){throw i=!1,this._$Ek(),t}i&&this._$AE(s)}willUpdate(t){}_$AE(t){var i;null===(i=this._$ES)||void 0===i||i.forEach((t=>{var i;return null===(i=t.hostUpdated)||void 0===i?void 0:i.call(t)})),this.hasUpdated||(this.hasUpdated=!0,this.firstUpdated(t)),this.updated(t)}_$Ek(){this._$AL=new Map,this.isUpdatePending=!1}get updateComplete(){return this.getUpdateComplete()}getUpdateComplete(){return this._$E_}shouldUpdate(t){return!0}update(t){void 0!==this._$EC&&(this._$EC.forEach(((t,i)=>this._$EO(i,this[i],t))),this._$EC=void 0),this._$Ek()}updated(t){}firstUpdated(t){}}
/**
 * @license
 * Copyright 2017 Google LLC
 * SPDX-License-Identifier: BSD-3-Clause
 */
var b;p.finalized=!0,p.elementProperties=new Map,p.elementStyles=[],p.shadowRootOptions={mode:"open"},null==u||u({ReactiveElement:p}),(null!==(l=h.reactiveElementVersions)&&void 0!==l?l:h.reactiveElementVersions=[]).push("1.6.1");const m=window,g=m.trustedTypes,y=g?g.createPolicy("lit-html",{createHTML:t=>t}):void 0,w="$lit$",$=`lit$${(Math.random()+"").slice(9)}$`,x="?"+$,S=`<${x}>`,A=document,k=()=>A.createComment(""),_=t=>null===t||"object"!=typeof t&&"function"!=typeof t,C=Array.isArray,j=t=>C(t)||"function"==typeof(null==t?void 0:t[Symbol.iterator]),E="[ \t\n\f\r]",O=/<(?:(!--|\/[^a-zA-Z])|(\/?[a-zA-Z][^>\s]*)|(\/?$))/g,U=/-->/g,T=/>/g,z=RegExp(`>|${E}(?:([^\\s"'>=/]+)(${E}*=${E}*(?:[^ \t\n\f\r"'\`<>=]|("|')|))|$)`,"g"),I=/'/g,M=/"/g,R=/^(?:script|style|textarea|title)$/i,N=(t=>(i,...s)=>({_$litType$:t,strings:i,values:s}))(1),D=Symbol.for("lit-noChange"),L=Symbol.for("lit-nothing"),P=new WeakMap,B=A.createTreeWalker(A,129,null,!1),H=(t,i)=>{const s=t.length-1,e=[];let n,o=2===i?"<svg>":"",r=O;for(let i=0;i<s;i++){const s=t[i];let l,h,a=-1,c=0;for(;c<s.length&&(r.lastIndex=c,h=r.exec(s),null!==h);)c=r.lastIndex,r===O?"!--"===h[1]?r=U:void 0!==h[1]?r=T:void 0!==h[2]?(R.test(h[2])&&(n=RegExp("</"+h[2],"g")),r=z):void 0!==h[3]&&(r=z):r===z?">"===h[0]?(r=null!=n?n:O,a=-1):void 0===h[1]?a=-2:(a=r.lastIndex-h[2].length,l=h[1],r=void 0===h[3]?z:'"'===h[3]?M:I):r===M||r===I?r=z:r===U||r===T?r=O:(r=z,n=void 0);const u=r===z&&t[i+1].startsWith("/>")?" ":"";o+=r===O?s+S:a>=0?(e.push(l),s.slice(0,a)+w+s.slice(a)+$+u):s+$+(-2===a?(e.push(void 0),i):u)}const l=o+(t[s]||"<?>")+(2===i?"</svg>":"");if(!Array.isArray(t)||!t.hasOwnProperty("raw"))throw Error("invalid template strings array");return[void 0!==y?y.createHTML(l):l,e]};class V{constructor({strings:t,_$litType$:i},s){let e;this.parts=[];let n=0,o=0;const r=t.length-1,l=this.parts,[h,a]=H(t,i);if(this.el=V.createElement(h,s),B.currentNode=this.el.content,2===i){const t=this.el.content,i=t.firstChild;i.remove(),t.append(...i.childNodes)}for(;null!==(e=B.nextNode())&&l.length<r;){if(1===e.nodeType){if(e.hasAttributes()){const t=[];for(const i of e.getAttributeNames())if(i.endsWith(w)||i.startsWith($)){const s=a[o++];if(t.push(i),void 0!==s){const t=e.getAttribute(s.toLowerCase()+w).split($),i=/([.?@])?(.*)/.exec(s);l.push({type:1,index:n,name:i[2],strings:t,ctor:"."===i[1]?Z:"?"===i[1]?G:"@"===i[1]?Q:W})}else l.push({type:6,index:n})}for(const i of t)e.removeAttribute(i)}if(R.test(e.tagName)){const t=e.textContent.split($),i=t.length-1;if(i>0){e.textContent=g?g.emptyScript:"";for(let s=0;s<i;s++)e.append(t[s],k()),B.nextNode(),l.push({type:2,index:++n});e.append(t[i],k())}}}else if(8===e.nodeType)if(e.data===x)l.push({type:2,index:n});else{let t=-1;for(;-1!==(t=e.data.indexOf($,t+1));)l.push({type:7,index:n}),t+=$.length-1}n++}}static createElement(t,i){const s=A.createElement("template");return s.innerHTML=t,s}}function q(t,i,s=t,e){var n,o,r,l;if(i===D)return i;let h=void 0!==e?null===(n=s._$Co)||void 0===n?void 0:n[e]:s._$Cl;const a=_(i)?void 0:i._$litDirective$;return(null==h?void 0:h.constructor)!==a&&(null===(o=null==h?void 0:h._$AO)||void 0===o||o.call(h,!1),void 0===a?h=void 0:(h=new a(t),h._$AT(t,s,e)),void 0!==e?(null!==(r=(l=s)._$Co)&&void 0!==r?r:l._$Co=[])[e]=h:s._$Cl=h),void 0!==h&&(i=q(t,h._$AS(t,i.values),h,e)),i}class J{constructor(t,i){this.u=[],this._$AN=void 0,this._$AD=t,this._$AM=i}get parentNode(){return this._$AM.parentNode}get _$AU(){return this._$AM._$AU}v(t){var i;const{el:{content:s},parts:e}=this._$AD,n=(null!==(i=null==t?void 0:t.creationScope)&&void 0!==i?i:A).importNode(s,!0);B.currentNode=n;let o=B.nextNode(),r=0,l=0,h=e[0];for(;void 0!==h;){if(r===h.index){let i;2===h.type?i=new K(o,o.nextSibling,this,t):1===h.type?i=new h.ctor(o,h.name,h.strings,this,t):6===h.type&&(i=new X(o,this,t)),this.u.push(i),h=e[++l]}r!==(null==h?void 0:h.index)&&(o=B.nextNode(),r++)}return n}p(t){let i=0;for(const s of this.u)void 0!==s&&(void 0!==s.strings?(s._$AI(t,s,i),i+=s.strings.length-2):s._$AI(t[i])),i++}}class K{constructor(t,i,s,e){var n;this.type=2,this._$AH=L,this._$AN=void 0,this._$AA=t,this._$AB=i,this._$AM=s,this.options=e,this._$Cm=null===(n=null==e?void 0:e.isConnected)||void 0===n||n}get _$AU(){var t,i;return null!==(i=null===(t=this._$AM)||void 0===t?void 0:t._$AU)&&void 0!==i?i:this._$Cm}get parentNode(){let t=this._$AA.parentNode;const i=this._$AM;return void 0!==i&&11===(null==t?void 0:t.nodeType)&&(t=i.parentNode),t}get startNode(){return this._$AA}get endNode(){return this._$AB}_$AI(t,i=this){t=q(this,t,i),_(t)?t===L||null==t||""===t?(this._$AH!==L&&this._$AR(),this._$AH=L):t!==this._$AH&&t!==D&&this.g(t):void 0!==t._$litType$?this.$(t):void 0!==t.nodeType?this.T(t):j(t)?this.k(t):this.g(t)}S(t){return this._$AA.parentNode.insertBefore(t,this._$AB)}T(t){this._$AH!==t&&(this._$AR(),this._$AH=this.S(t))}g(t){this._$AH!==L&&_(this._$AH)?this._$AA.nextSibling.data=t:this.T(A.createTextNode(t)),this._$AH=t}$(t){var i;const{values:s,_$litType$:e}=t,n="number"==typeof e?this._$AC(t):(void 0===e.el&&(e.el=V.createElement(e.h,this.options)),e);if((null===(i=this._$AH)||void 0===i?void 0:i._$AD)===n)this._$AH.p(s);else{const t=new J(n,this),i=t.v(this.options);t.p(s),this.T(i),this._$AH=t}}_$AC(t){let i=P.get(t.strings);return void 0===i&&P.set(t.strings,i=new V(t)),i}k(t){C(this._$AH)||(this._$AH=[],this._$AR());const i=this._$AH;let s,e=0;for(const n of t)e===i.length?i.push(s=new K(this.S(k()),this.S(k()),this,this.options)):s=i[e],s._$AI(n),e++;e<i.length&&(this._$AR(s&&s._$AB.nextSibling,e),i.length=e)}_$AR(t=this._$AA.nextSibling,i){var s;for(null===(s=this._$AP)||void 0===s||s.call(this,!1,!0,i);t&&t!==this._$AB;){const i=t.nextSibling;t.remove(),t=i}}setConnected(t){var i;void 0===this._$AM&&(this._$Cm=t,null===(i=this._$AP)||void 0===i||i.call(this,t))}}class W{constructor(t,i,s,e,n){this.type=1,this._$AH=L,this._$AN=void 0,this.element=t,this.name=i,this._$AM=e,this.options=n,s.length>2||""!==s[0]||""!==s[1]?(this._$AH=Array(s.length-1).fill(new String),this.strings=s):this._$AH=L}get tagName(){return this.element.tagName}get _$AU(){return this._$AM._$AU}_$AI(t,i=this,s,e){const n=this.strings;let o=!1;if(void 0===n)t=q(this,t,i,0),o=!_(t)||t!==this._$AH&&t!==D,o&&(this._$AH=t);else{const e=t;let r,l;for(t=n[0],r=0;r<n.length-1;r++)l=q(this,e[s+r],i,r),l===D&&(l=this._$AH[r]),o||(o=!_(l)||l!==this._$AH[r]),l===L?t=L:t!==L&&(t+=(null!=l?l:"")+n[r+1]),this._$AH[r]=l}o&&!e&&this.j(t)}j(t){t===L?this.element.removeAttribute(this.name):this.element.setAttribute(this.name,null!=t?t:"")}}class Z extends W{constructor(){super(...arguments),this.type=3}j(t){this.element[this.name]=t===L?void 0:t}}const F=g?g.emptyScript:"";class G extends W{constructor(){super(...arguments),this.type=4}j(t){t&&t!==L?this.element.setAttribute(this.name,F):this.element.removeAttribute(this.name)}}class Q extends W{constructor(t,i,s,e,n){super(t,i,s,e,n),this.type=5}_$AI(t,i=this){var s;if((t=null!==(s=q(this,t,i,0))&&void 0!==s?s:L)===D)return;const e=this._$AH,n=t===L&&e!==L||t.capture!==e.capture||t.once!==e.once||t.passive!==e.passive,o=t!==L&&(e===L||n);n&&this.element.removeEventListener(this.name,this,e),o&&this.element.addEventListener(this.name,this,t),this._$AH=t}handleEvent(t){var i,s;"function"==typeof this._$AH?this._$AH.call(null!==(s=null===(i=this.options)||void 0===i?void 0:i.host)&&void 0!==s?s:this.element,t):this._$AH.handleEvent(t)}}class X{constructor(t,i,s){this.element=t,this.type=6,this._$AN=void 0,this._$AM=i,this.options=s}get _$AU(){return this._$AM._$AU}_$AI(t){q(this,t)}}const Y={P:w,A:$,M:x,C:1,L:H,D:J,R:j,V:q,I:K,H:W,N:G,U:Q,F:Z,B:X},tt=m.litHtmlPolyfillSupport;null==tt||tt(V,K),(null!==(b=m.litHtmlVersions)&&void 0!==b?b:m.litHtmlVersions=[]).push("2.7.0");
/**
 * @license
 * Copyright 2017 Google LLC
 * SPDX-License-Identifier: BSD-3-Clause
 */
var it,st;class et extends p{constructor(){super(...arguments),this.renderOptions={host:this},this._$Do=void 0}createRenderRoot(){var t,i;const s=super.createRenderRoot();return null!==(t=(i=this.renderOptions).renderBefore)&&void 0!==t||(i.renderBefore=s.firstChild),s}update(t){const i=this.render();this.hasUpdated||(this.renderOptions.isConnected=this.isConnected),super.update(t),this._$Do=((t,i,s)=>{var e,n;const o=null!==(e=null==s?void 0:s.renderBefore)&&void 0!==e?e:i;let r=o._$litPart$;if(void 0===r){const t=null!==(n=null==s?void 0:s.renderBefore)&&void 0!==n?n:null;o._$litPart$=r=new K(i.insertBefore(k(),t),t,void 0,null!=s?s:{})}return r._$AI(t),r})(i,this.renderRoot,this.renderOptions)}connectedCallback(){var t;super.connectedCallback(),null===(t=this._$Do)||void 0===t||t.setConnected(!0)}disconnectedCallback(){var t;super.disconnectedCallback(),null===(t=this._$Do)||void 0===t||t.setConnected(!1)}render(){return D}}et.finalized=!0,et._$litElement$=!0,null===(it=globalThis.litElementHydrateSupport)||void 0===it||it.call(globalThis,{LitElement:et});const nt=globalThis.litElementPolyfillSupport;null==nt||nt({LitElement:et}),(null!==(st=globalThis.litElementVersions)&&void 0!==st?st:globalThis.litElementVersions=[]).push("3.3.0");
/**
 * @license
 * Copyright 2017 Google LLC
 * SPDX-License-Identifier: BSD-3-Clause
 */
const ot=t=>i=>"function"==typeof i?((t,i)=>(customElements.define(t,i),i))(t,i):((t,i)=>{const{kind:s,elements:e}=i;return{kind:s,elements:e,finisher(i){customElements.define(t,i)}}})(t,i)
/**
 * @license
 * Copyright 2017 Google LLC
 * SPDX-License-Identifier: BSD-3-Clause
 */,rt=(t,i)=>"method"===i.kind&&i.descriptor&&!("value"in i.descriptor)?{...i,finisher(s){s.createProperty(i.key,t)}}:{kind:"field",key:Symbol(),placement:"own",descriptor:{},originalKey:i.key,initializer(){"function"==typeof i.initializer&&(this[i.key]=i.initializer.call(this))},finisher(s){s.createProperty(i.key,t)}};function lt(t){return(i,s)=>void 0!==s?((t,i,s)=>{i.constructor.createProperty(s,t)})(t,i,s):rt(t,i)
/**
 * @license
 * Copyright 2017 Google LLC
 * SPDX-License-Identifier: BSD-3-Clause
 */}function ht(t){return lt({...t,state:!0})}
/**
 * @license
 * Copyright 2021 Google LLC
 * SPDX-License-Identifier: BSD-3-Clause
 */var at;null===(at=window.HTMLSlotElement)||void 0===at||at.prototype.assignedElements;var ct=function(t,i,s,e){for(var n,o=arguments.length,r=o<3?i:null===e?e=Object.getOwnPropertyDescriptor(i,s):e,l=t.length-1;l>=0;l--)(n=t[l])&&(r=(o<3?n(r):o>3?n(i,s,r):n(i,s))||r);return o>3&&r&&Object.defineProperty(i,s,r),r};let ut=class extends et{constructor(){super(...arguments),this.linePrefix="AA",this.prefixIcon="bus-front-fill",this.lineSuffix="IDA",this.direction="I",this._clickCallback=()=>{},this.lineData=null,this.routeName=null,this.routeId=null,this.stopsData=null,this.stopsCoordinates=[]}render(){return N`
      <div class="line-container">
        <sl-button @click="${this._clickCallback}" style="width: 100%;" class="line-button" variant="default" size="medium">
          <div slot="prefix" class="line-prefix">
            <sl-icon slot="prefix" name="${this.prefixIcon}"></sl-icon>
            <span>${this.linePrefix}</span>
          </div>
          <slot></slot>
          <div slot="suffix" class="line-suffix">
            <sl-tag variant="${"I"===this.direction?"primary":"danger"}" size="small" pill>${this.lineSuffix}</sl-tag>
          </div>
        </sl-button>
        <sl-divider></sl-divider>
      </div>
    `}};ut.styles=o`
    .line-container .line-prefix {
      color: tomato;
      font-size: large;
    }

    .line-container .line-prefix>span {
      font-weight: var(--sl-font-weight-semibold);
      font-family: var(--sl-font-mono);
      padding-left: var(--sl-spacing-2x-small);
    }

    .line-container .line-suffix {
      font-size: smaller;
    }

    sl-button.line-button::part(base) {
      border-style: none;
      justify-content: start;
      align-items: center;
    }

    sl-button.line-button::part(label) {
      white-space: normal;
      line-height: var(--sl-line-height-denser)
    }

    sl-button.line-button::part(suffix) {
      flex-grow: 1;
      justify-content: end;
    }
  `,ct([lt()],ut.prototype,"linePrefix",void 0),ct([lt()],ut.prototype,"prefixIcon",void 0),ct([lt()],ut.prototype,"lineSuffix",void 0),ct([lt()],ut.prototype,"direction",void 0),ct([lt()],ut.prototype,"_clickCallback",void 0),ut=ct([ot("tomate-line")],ut);
/**
 * @license
 * Copyright 2017 Google LLC
 * SPDX-License-Identifier: BSD-3-Clause
 */
const dt=2;class vt{constructor(t){}get _$AU(){return this._$AM._$AU}_$AT(t,i,s){this._$Ct=t,this._$AM=i,this._$Ci=s}_$AS(t,i){return this.update(t,i)}update(t,i){return this.render(...i)}}
/**
 * @license
 * Copyright 2020 Google LLC
 * SPDX-License-Identifier: BSD-3-Clause
 */const{I:ft}=Y,pt=()=>document.createComment(""),bt=(t,i,s)=>{var e;const n=t._$AA.parentNode,o=void 0===i?t._$AB:i._$AA;if(void 0===s){const i=n.insertBefore(pt(),o),e=n.insertBefore(pt(),o);s=new ft(i,e,t,t.options)}else{const i=s._$AB.nextSibling,r=s._$AM,l=r!==t;if(l){let i;null===(e=s._$AQ)||void 0===e||e.call(s,t),s._$AM=t,void 0!==s._$AP&&(i=t._$AU)!==r._$AU&&s._$AP(i)}if(i!==o||l){let t=s._$AA;for(;t!==i;){const i=t.nextSibling;n.insertBefore(t,o),t=i}}}return s},mt=(t,i,s=t)=>(t._$AI(i,s),t),gt={},yt=t=>{var i;null===(i=t._$AP)||void 0===i||i.call(t,!1,!0);let s=t._$AA;const e=t._$AB.nextSibling;for(;s!==e;){const t=s.nextSibling;s.remove(),s=t}},wt=(t,i,s)=>{const e=new Map;for(let n=i;n<=s;n++)e.set(t[n],n);return e},$t=(t=>(...i)=>({_$litDirective$:t,values:i}))(class extends vt{constructor(t){if(super(t),t.type!==dt)throw Error("repeat() can only be used in text expressions")}ht(t,i,s){let e;void 0===s?s=i:void 0!==i&&(e=i);const n=[],o=[];let r=0;for(const i of t)n[r]=e?e(i,r):r,o[r]=s(i,r),r++;return{values:o,keys:n}}render(t,i,s){return this.ht(t,i,s).values}update(t,[i,s,e]){var n;const o=(t=>t._$AH)(t),{values:r,keys:l}=this.ht(i,s,e);if(!Array.isArray(o))return this.ut=l,r;const h=null!==(n=this.ut)&&void 0!==n?n:this.ut=[],a=[];let c,u,d=0,v=o.length-1,f=0,p=r.length-1;for(;d<=v&&f<=p;)if(null===o[d])d++;else if(null===o[v])v--;else if(h[d]===l[f])a[f]=mt(o[d],r[f]),d++,f++;else if(h[v]===l[p])a[p]=mt(o[v],r[p]),v--,p--;else if(h[d]===l[p])a[p]=mt(o[d],r[p]),bt(t,a[p+1],o[d]),d++,p--;else if(h[v]===l[f])a[f]=mt(o[v],r[f]),bt(t,o[d],o[v]),v--,f++;else if(void 0===c&&(c=wt(l,f,p),u=wt(h,d,v)),c.has(h[d]))if(c.has(h[v])){const i=u.get(l[f]),s=void 0!==i?o[i]:null;if(null===s){const i=bt(t,o[d]);mt(i,r[f]),a[f]=i}else a[f]=mt(s,r[f]),bt(t,o[d],s),o[i]=null;f++}else yt(o[v]),v--;else yt(o[d]),d++;for(;f<=p;){const i=bt(t,a[p+1]);mt(i,r[f]),a[f++]=i}for(;d<=v;){const t=o[d++];null!==t&&yt(t)}return this.ut=l,((t,i=gt)=>{t._$AH=i})(t,a),D}});
/**
 * @license
 * Copyright 2017 Google LLC
 * SPDX-License-Identifier: BSD-3-Clause
 */var xt=function(t,i,s,e){for(var n,o=arguments.length,r=o<3?i:null===e?e=Object.getOwnPropertyDescriptor(i,s):e,l=t.length-1;l>=0;l--)(n=t[l])&&(r=(o<3?n(r):o>3?n(i,s,r):n(i,s))||r);return o>3&&r&&Object.defineProperty(i,s,r),r};let St=class extends et{constructor(){super(),this.lines=[],this.filteredLines=[],this.query="",this._clickCallback=(t,i,s)=>{},this.filteredLines=[...this.lines]}willUpdate(t){0!=this.filteredLines.length||this.query.trim().toLowerCase()||(this.filteredLines=[...this.lines])}_filter(t){this.query=t.trim().toLowerCase(),this.query?this.filteredLines=this.lines.filter((t=>{const i=t.lineData.name.replace("R","Ramal").replace("T","Troncal").replace("D","Domingos");return t.routeData.name.toLowerCase().includes(this.query)||i.toLowerCase().includes(this.query)})):this.filteredLines=this.lines}render(){return N`
      <div>
      ${$t(this.filteredLines,(t=>t.routeData.id),(t=>{const i=new ut;i.innerText=t.routeData.name,i.linePrefix=t.lineData.name.replaceAll(" ","");try{i.direction=t.routeData.direction,i.lineSuffix="I"===t.routeData.direction?"IDA":"VUELTA",i.lineData=t.lineData,i.routeName=t.routeData.name,i.routeId=t.routeData.id,i.stopsData=t.stopsData.stops,i.stopsCoordinates=t.stopsData.route,i._clickCallback=()=>{this._clickCallback(t.routeData,t.lineData,t.stopsData)}}catch(t){return N`<span>${t}</span>`}return i}))}
      </div>
    `}};St.styles=o`
    .line-items::marker {
      display:none;
    }
  `,xt([lt()],St.prototype,"lines",void 0),xt([ht()],St.prototype,"filteredLines",void 0),xt([ht()],St.prototype,"query",void 0),xt([lt()],St.prototype,"_clickCallback",void 0),St=xt([ot("tomate-lines")],St);var At=function(t,i,s,e){for(var n,o=arguments.length,r=o<3?i:null===e?e=Object.getOwnPropertyDescriptor(i,s):e,l=t.length-1;l>=0;l--)(n=t[l])&&(r=(o<3?n(r):o>3?n(i,s,r):n(i,s))||r);return o>3&&r&&Object.defineProperty(i,s,r),r};let kt=class extends et{render(){return N`
      <div class="tomate-route-header">
        <div class="route-name-container">
          <sl-icon name="info-circle" label="Información sobre paradas." style="display: none;"></sl-icon>
          <slot></slot>
        </div>
      </div>
    `}};kt.styles=o`
    sl-button.change-direction-button::part(label) {
      padding-left: 0;
    }

    .route-name-container {
      align-items: start;
      display: flex;
      flex-direction: column;
    }

    .route-name-container>sl-tooltip>sl-icon {
      color: var(--sl-color-primary-600)
    }

    .route-name-container>sl-tooltip>sl-icon:hover {
      color: var(--sl-color-primary-500)
    }

    .tomate-route-header {
      display: flex;
      flex-direction: column;
      color: var(--sl-color-gray-700);
      align-items: start;
      font-size: var(--sl-font-size-large);
      font-weight: var(--sl-font-weight-bold);
      font-family: var(--sl-font-sans);
      margin-top: var(--sl-spacing-medium);
      padding: var(--sl-spacing-x-small);
      border-style: solid;
      border-left: none;
      border-right: none;
      border-bottom-color: var(--sl-color-gray-300);
      border-top-color: var(--sl-color-gray-300);
      border-width: var(--sl-input-border-width);
    }
  `,kt=At([ot("tomate-route-header")],kt);var _t=kt,Ct=function(t,i,s,e){for(var n,o=arguments.length,r=o<3?i:null===e?e=Object.getOwnPropertyDescriptor(i,s):e,l=t.length-1;l>=0;l--)(n=t[l])&&(r=(o<3?n(r):o>3?n(i,s,r):n(i,s))||r);return o>3&&r&&Object.defineProperty(i,s,r),r};let jt=class extends et{constructor(){super(...arguments),this.prefixLabel=""}render(){return N`
      <sl-button variant="default" size="medium" class="tomate-back-button">
        <div slot="prefix" class="tomate-back-button-prefix">
          <sl-icon name="chevron-left"></sl-icon>
          <span class="tomate-back-button-icon-prefix">${this.prefixLabel}</span>
        </div>
        <slot></slot>
      </sl-button>
    `}};jt.styles=o`
    .tomate-back-button-icon-prefix {
      color: tomato;
      font-weight: var(--sl-font-weight-semibold);
      font-family: var(--sl-font-mono);
      font-size: var(--sl-font-size-medium);
    }

    .tomate-back-button-prefix {
      display: flex;
      align-items: center;
    }

    sl-button.tomate-back-button::part(base) {
      border-style: none;
    }
  `,Ct([lt()],jt.prototype,"prefixLabel",void 0),jt=Ct([ot("tomate-back-button")],jt);var Et=jt;export{Et as TomateBackButton,ut as TomateLine,St as TomateLines,_t as TomateRouteHeader};
