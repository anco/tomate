import {
  SlIcon
} from "./chunk.YU2QRT77.js";

// src/react/icon/index.ts
import * as React from "react";
import { createComponent } from "@lit-labs/react";
var icon_default = createComponent({
  tagName: "sl-icon",
  elementClass: SlIcon,
  react: React,
  events: {
    onSlLoad: "sl-load",
    onSlError: "sl-error"
  }
});

export {
  icon_default
};
