import {
  SlSpinner
} from "./chunk.7DBJ5ZVU.js";

// src/react/spinner/index.ts
import * as React from "react";
import { createComponent } from "@lit-labs/react";
var spinner_default = createComponent({
  tagName: "sl-spinner",
  elementClass: SlSpinner,
  react: React,
  events: {}
});

export {
  spinner_default
};
